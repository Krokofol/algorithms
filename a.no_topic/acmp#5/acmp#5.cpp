#include <iostream>

using namespace std;

int main(){
    int n, i3 = 0, i4 = 0, i, input;
    cin >> n;
    int *array_4 = new int[n];
    int *array_3 = new int[n];
    
    for (i = 0; i < n; i++){
        cin >> input;
        if (input % 2) {
            array_3[i3] = input;
            i3++;
        }
        else {
            array_4[i4] = input;
            i4++;
        }
    }

    for (i = 0; i < i3; i++){
        cout << array_3[i] << " ";
    }
    cout << "\n";
    for (i = 0; i < i4; i++){
        cout << array_4[i] << " ";
    }
    cout << "\n";

    if (i4 >= i3)
        cout << "YES";
    else
        cout << "NO";

    return 0;
}