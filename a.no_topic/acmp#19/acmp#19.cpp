#include <iostream>

using namespace std;

int main(){
    bool field[8][8];
    int i, j, counter = 0;
    for (i = 0; i < 64; i++){
        field[i >> 1][i & 1] = 0;
    }

    char Space;
    char str[8];
    cin >> str;
    char coord[3][2];

    coord[0][0] = str[0] - 'A';
    coord[0][1] = str[1] - 1;
    coord[1][0] = str[3] - 'A';
    coord[1][1] = str[4] - 1;
    coord[2][0] = str[6] - 'A';
    coord[2][1] = str[7] - 1;

    field[coord[2][0]][coord[2][1]] = true;
    for (j = 0; j < 2; j++)
        for (i = 0; i < 8; i++)
        {
            field[i][coord[j][1]] = true;
            field[coord[j][0]][i] = true;
        }
    int x = coord[0][0], y= coord[0][1];
    while (x >= 0 && y >=0){
        field[x][y] = true;
        x--;
        y--;
    }
    x = coord[0][0];
    y = coord[0][1];
    while (x >= 0 && y >=0){
        field[x][y] = true;
        x++;
        y++;
    }
    x = coord[0][0];
    y = coord[0][1];
    while (x >= 0 && y >=0){
        field[x][y] = true;
        x--;
        y++;
    }
    x = coord[0][0];
    y = coord[0][1];
    while (x >= 0 && y >=0){
        field[x][y] = true;
        x++;
        y--;
    }

    x = coord[2][0];
    y = coord[2][1];
    
    if (x + 2 < 8){
        if (y - 1 > 0)
            field[x + 2][y - 1] = true;
        if (y + 1 < 8)
            field[x + 2][y + 1] = true;
    }
    if (x - 2 < 8){
        if (y - 1 > 0)
            field[x - 2][y - 1] = true;
        if (y + 1 < 8)
            field[x - 2][y + 1] = true;
    }
    if (y + 2 < 8){
        if (x - 1 > 0)
            field[x - 1][y + 2] = true;
        if (x + 1 < 8)
            field[x + 1][y + 2] = true;
    }
    if (y - 2 < 8){
        if (x - 1 > 0)
            field[x - 1][y - 2] = true;
        if (x + 1 < 8)
            field[x + 1][y - 2] = true;
    }

    for (i = 0; i < 8; i++)
        for (j = 0; j < 8; j++)
            if (field[i][j])
                counter++;

    cout << counter - 3;

    return 0;
}