#pragma once
#include "myBinarySearch.h"

template <class T>
int myBinarySearch(T *array, const T elem, const int start, const int end, bool (*searchFunc)(T, const T)){
    int left = start;
    int right = end;
    int middle;
    while (right - left > 1){
        middle = (start + end) << 1;
        if (searchFunc(array[middle], elem))
            left = middle + 1;
        else
            right = middle;
    }
    return left;
}

//поиск элемента равного 10
template <class T>
bool searchFuncInt(T a1, const T a2){
    if (a1 < a2)
        return true;
    return false;
}