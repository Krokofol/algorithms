#pragma once

//myBinarySearch : массив, условие поиска
template <class T>
int myBinarySearch(T *array, const T elem, const int start, const int end, bool (*searchFunc)(T, const T));