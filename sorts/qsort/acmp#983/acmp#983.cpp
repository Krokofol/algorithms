#include <iostream>
#include <cstring>
using namespace std;
template <class T>
void myswap(T* array, int i1, int i2) {
	T boof = array[i1];
	array[i1] = array[i2];
	array[i2] = boof;
	return;
}
template <class T>
void myqsort(T* array, const int start, const int end, bool (*compareFunc)(T, T)) {
	int i[2] = { start, end };
	while (i[0] != i[1]) {
		if (i[0] < i[1] && compareFunc(array[i[0]], array[i[1]])) {
			myswap<T>(array, i[0], i[1]);
			myswap<int>(i, 0, 1);
		}
		if (i[0] > i[1] && compareFunc(array[i[1]], array[i[0]])) {
			myswap<T>(array, i[0], i[1]);
			myswap<int>(i, 0, 1);
		}
		if (i[0] < i[1])
			i[1]--;
		else
			i[1]++;
	}
	if (i[0] - 1 > start)
		myqsort<T>(array, start, i[0] - 1, compareFunc);
	if (i[0] + 1 < end)
		myqsort<T>(array, i[0] + 1, end, compareFunc);
	return;
}

struct people{
    long long pos;
    long long spead;
	int index;
	long long time;
};

bool mycompare1(people a1, people a2){
    return a1.pos > a2.pos;
}
bool mycompare2(people a1, people a2){
    return a1.index > a2.index;
}

int main(){
    int n, i;
	long long time;
	people *array;
    cin >> n;
    array = new people[n];
    for (i = 0; i < n; i++){
        cin >> array[i].spead >> array[i].pos;
		array[i].index = i;
	}
    
	myqsort(array, 0, n - 1, mycompare1);
    
	time = 0;
    for (i = 0; i < n; i++){
        if (time < array[i].pos * array[i].spead){
            time = array[i].pos * array[i].spead;
        }
		array[i].time = time;
	}
	
	myqsort(array, 0, n - 1, mycompare2);

	for (i = 0; i < n; i++){
		cout << array[i].time << '\n';
	}

	return 0;
}
