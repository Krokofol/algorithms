#include "..\myqsort.cpp"
#include <iostream>
using namespace std;

int main(){
    int n;
    
    cout << "enter the number of elements in the double array\n";
    cin >> n;
    cout << "enter " << n << " elements\n";
    
    double *array = new double[n];
    for (int i = 0; i < n; i++){
        cin >> array[i];
    }

    myqsort<double>(array, 0, n - 1, compareMax);
    
    for (int i = 0; i < n; i++){
        cout << array[i] << " ";
    }
}