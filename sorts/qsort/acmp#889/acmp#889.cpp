#include <iostream>
#include <cstring>
using namespace std;



template <class T>
void myswap(T* array, int i1, int i2) {
	T boof = array[i1];
	array[i1] = array[i2];
	array[i2] = boof;
	return;
}
template <class T>
void myqsort(T* array, const int start, const int end, bool (*compareFunc)(T, T)) {
	int i[2] = { start, end };
	while (i[0] != i[1]) {
		if (i[0] < i[1] && compareFunc(array[i[0]], array[i[1]])) {
			myswap<T>(array, i[0], i[1]);
			myswap<int>(i, 0, 1);
		}
		if (i[0] > i[1] && compareFunc(array[i[1]], array[i[0]])) {
			myswap<T>(array, i[0], i[1]);
			myswap<int>(i, 0, 1);
		}
		if (i[0] < i[1])
			i[1]--;
		else
			i[1]++;
	}
	if (i[0] - 1 > start)
		myqsort<T>(array, start, i[0] - 1, compareFunc);
	if (i[0] + 1 < end)
		myqsort<T>(array, i[0] + 1, end, compareFunc);
	return;
}

struct line {
	int p1;
	int p2;
	int hight;
};

bool mycompare(line a1, line a2) {
	return a1.hight < a2.hight;
}

int main() {
	line* array;
	int pos, n;
	int hight = 0;

	cin >> pos >> n;
	array = (line*)malloc(sizeof(line) * n);
	for (int i = 0; i < n; i++) {
		cin >> array[i].p1 >> array[i].hight;
		array[i].p2 = array[i].p1 + 1;
		if (array[i].hight > hight)
			hight = array[i].hight;
	}
	myqsort<line>(array, 0, n - 1, mycompare);

	for (int i = 0; i < n; i++) {
		if (pos == array[i].p1 && hight >= array[i].hight) {
			pos = array[i].p2;
			hight = array[i].hight;
			continue;
		}
		if (pos == array[i].p2 && hight >= array[i].hight) {
			pos = array[i].p1;
			hight = array[i].hight;
			continue;
		}
	}
	cout << pos;
	return 0;
}