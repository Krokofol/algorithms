#pragma once
#include "myqsort.h"

template <class T>
void myswap(T* array, int i1, int i2){
    T boof = array[i1];
    array[i1] = array[i2];
    array[i2] = boof;
    return;
}

template <class T>
void myqsort(T *array, const int start, const int end, bool (*compareFunc)(T, T)){
    int i[2] = {start, end};
    while (i[0] != i[1]){
        if (i[0] < i[1] && compareFunc(array[i[0]], array[i[1]])){
            myswap<T>(array, i[0], i[1]);
            myswap<int>(i, 0, 1);
        }
        if (i[0] > i[1] && compareFunc(array[i[1]], array[i[0]])){
            myswap<T>(array, i[0], i[1]);
            myswap<int>(i, 0, 1);
        }
        if (i[0] < i[1])
            i[1]--;
        else
            i[1]++;
    }
    if (i[0] - 1 > start)
        myqsort<T>(array, start, i[0] - 1, compareFunc);
    if (i[0] + 1 < end)
        myqsort<T>(array, i[0] + 1, end, compareFunc);
    return;
}

bool compareMin(double a1, double a2) {
    return a1 > a2;
}
bool compareMax(double a1, double a2) {
    return a1 < a2;
}
bool compareStringsMin(char *a1, char *a2){
    return strlen(a1) > strlen(a2);
}
bool compareStringsMax(char *a1, char *a2){
    return strlen(a1) < strlen(a2);
}