#include <iostream>
#include <cstring>
#include <cstring>
using namespace std;
template <class T>
void myswap(T* array, int i1, int i2) {
	T boof = array[i1];
	array[i1] = array[i2];
	array[i2] = boof;
	return;
}
template <class T>
void myqsort(T* array, const int start, const int end, bool (*compareFunc)(T, T)) {
	int i[2] = { start, end };
	while (i[0] != i[1]) {
		if (i[0] < i[1] && compareFunc(array[i[0]], array[i[1]])) {
			myswap<T>(array, i[0], i[1]);
			myswap<int>(i, 0, 1);
		}
		if (i[0] > i[1] && compareFunc(array[i[1]], array[i[0]])) {
			myswap<T>(array, i[0], i[1]);
			myswap<int>(i, 0, 1);
		}
		if (i[0] < i[1])
			i[1]--;
		else
			i[1]++;
	}
	if (i[0] - 1 > start)
		myqsort<T>(array, start, i[0] - 1, compareFunc);
	if (i[0] + 1 < end)
		myqsort<T>(array, i[0] + 1, end, compareFunc);
	return;
}

struct Word {
	char* word;
	char* group;
	int* groupCounter;
};

bool compareGroup(char a1, char a2) {
	return a1 > a2;
}
bool compareBig1(Word a1, Word a2) {
	int length1 = strlen(a1.word);
	int length2 = strlen(a2.word);
	int i;
	//сортировка по длине
	if (length1 != length2)
		return length1 > length2;
	//сортировка по группе
	for (i = 0; i < length1; i++) {
		if (a1.group[i] != a2.group[i])
			return a1.group[i] > a2.group[i];
	}
	//сортировка втнутри группы
	for (i = 0; i < length1; i++) {
		if (a1.word[i] != a2.word[i])
			return a1.word[i] > a2.word[i];
	}
	return false;
}
bool compareBig2(Word a1, Word a2) {
	if (*a1.groupCounter > *a2.groupCounter) return true;
    if (a1.groupCounter != a2.)
}

bool compareBig3(Word a1, Word a2) {
	if (*a1.groupCounter != * a2.groupCounter)
        return false;
    for (int i = 0; i < strlen(a1.word); i++){
        if (a1.word[i] != a2.word[i])
            return a1.word[i] < a2.word[i];
    }
    return false;
}


int main() {
	int n, i, j, k, length1, length2, groupNumber, groupCounter[25000];
	int number, * pointer;
	Word* array;
	Word** groupedArray;
	char input[43], group[43], word[43];
	bool got;

	cin >> n;
	array = new Word[n];

	for (i = 0; i < n; i++) {
		cin >> input;
		length1 = strlen(input);
		array[i].word = new char[length1 + 1];
		array[i].group = new char[length1 + 1];
		for (j = 0; j < length1; j++) {
			array[i].word[j] = input[j];
			array[i].group[j] = input[j];
		}
		array[i].word[j] = '\0';
		array[i].group[j] = '\0';
		myqsort<char>(array[i].group, 0, length1 - 1, compareGroup);
	}

	myqsort(array, 0, n - 1, compareBig1);

	groupNumber = 0;
	length1 = strlen(array[0].word);
	for (i = 0; i < length1; i++)
		group[i] = array[0].group[i];
	for (i = 0; i < n; i++) {
		length2 = strlen(array[i].word);
		if (length1 != length2) {
			length1 = length2;
			groupNumber++;
			groupCounter[groupNumber]++;
			array[i].groupCounter = &groupCounter[groupNumber];
			for (j = 0; j < length1; j++)
				group[j] = array[i].group[j];
			continue;
		}
		got = false;
		for (j = 0; j < length1; j++)
			if (group[j] != array[i].group[j]) {
				got = true;
				break;
			}
		if (got) {
			length1 = length2;
			groupNumber++;
			groupCounter[groupNumber]++;
			array[i].groupCounter = &groupCounter[groupNumber];
			for (j = 0; j < length1; j++)
				group[j] = array[i].group[j];
			continue;
		}
		else {
			groupCounter[groupNumber]++;
			array[i].groupCounter = &groupCounter[groupNumber];
		}
	}

	myqsort(array, 0, n - 1, compareBig2);
    myqsort(array, 0, n - 1, compareBig3);

    number = 0;
	i = n - 1;
	length1 = strlen(array[i].word);
	pointer = array[i].groupCounter;
    cout << "Group of size " << *pointer << ": ";
	while (number < 5 && i >= 0) {
		length2 = strlen(array[i].word);
		if (pointer != array[i].groupCounter) {
			number++;
			pointer = array[i].groupCounter;
			cout << ".\nGroup of size " << *pointer << ": ";
		}
        got = false;
        for (k = 0; k < length1; k++){
            if (word[k] != array[i].word[k])
                got = true;
        }
        for (k = 0; k < length1; k++){
            word[k] = array[i].word[k];
        }
		if (got) cout << array[i].word << " ";
		i--;
	}
	cout << ".";

	return 0;
}

