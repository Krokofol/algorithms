#pragma once
#include <cstring>

//swap : массив, индексы элементов которые надо поменять
template <class T>
void myswap(T *array, int i1, int i2);

//myqsort : массив, индексы начала и конца, функция сравнения
template <class T>
void myqsort(T *array, const int start, const int end, bool (*compareFunc)(T, T));

//compare : sort array(int, double, char ...) from min to max
bool compareMin(double a1, double a2);
//compare : sort array(int, double, char ...) from max to min
bool compareMax(double a1, double a2);
//compare : sort string array from min to max
bool compareStringsMin(char *a1, char *a2);
//compare : sort string array from max to min
bool compareStringsMax(char *a1, char *a2);
